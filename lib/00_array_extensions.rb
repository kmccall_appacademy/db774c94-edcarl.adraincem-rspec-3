require 'byebug'
# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    added_int = 0
    self.each { |num| added_int += num }
    added_int
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    self.each_with_index { |num, idx| self[idx] = num**2 }
  end

  def square
    squared_arr = []
    self.each_with_index { |num| squared_arr << num**2 }
    squared_arr
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    uniq_arr = []
    self.each do |el|
      next if uniq_arr.include?(el)
      uniq_arr << el
    end
    uniq_arr
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)

class Array
  def two_sum
    zero_pair = []
    (0...self.length).each do |idx1|
      (idx1 + 1...self.length).each do |idx2|
        if self[idx1] + self[idx2] == 0
          zero_pair << [idx1, idx2]
        end
      end
    end

    zero_pair
  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median
    sorted_arr = self.sort
    length = self.length

    if self.empty?
      return nil
    elsif length.even?
      idx1 = (length / 2) - 1
      idx2 = idx1 + 1
      (sorted_arr[idx1] + sorted_arr[idx2]) / 2.0
    elsif length.odd?
      sorted_arr[(length / 2)]
    end
  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

=begin
row[0][0] = row[0][0]
row[0][1] = row[1][0]
row[0][2] = row[2][0]

row[1][0] = row[0][1]
row[1][1] = row[1][1]
row[1][2] = row[2][1]

row[2][0] = row[0][2]
row[2][1] = row[1][2]
row[2][2] = row[2][2]
=end

class Array
  def my_transpose
    new_matrix = []
    self.length.times do |idx1|
      row_arr = []
      self.length.times do |idx2|
        row_arr << self[idx2][idx1]
      end
      new_matrix << row_arr
    end
    new_matrix
  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
